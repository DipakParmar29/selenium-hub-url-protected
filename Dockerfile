FROM seleniarm/hub:latest

USER root

COPY append-basic-auth /opt/bin/
COPY start-selenium-grid-hub.sh /opt/bin/

RUN cat /opt/bin/append-basic-auth >> /opt/bin/generate_config && chmod 755 /opt/bin/generate_config


